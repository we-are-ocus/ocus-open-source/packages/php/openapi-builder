<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Merge;

use cebe\openapi\spec\Paths;
use Symfony\Component\Console\Output\OutputInterface;

interface PathMergerInterface
{
    /**
     * @param Paths $existingPaths
     * @param Paths $newPaths
     * @param OutputInterface $output
     * @param string $resolveConflicts
     * @return Paths
     */
    public function mergePaths(
        Paths $existingPaths,
        Paths $newPaths,
        OutputInterface $output,
        string $resolveConflicts = 'fail_on_conflict'
    ): Paths;
}
