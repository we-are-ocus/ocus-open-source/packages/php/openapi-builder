<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Merge;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\spec\Paths;
use Symfony\Component\Console\Output\OutputInterface;

class PathMerger implements PathMergerInterface
{
    private const PATH_OPERATIONS = [
        'get',
        'put',
        'post',
        'delete',
        'options',
        'head',
        'patch',
        'trace',
    ];

    /**
     * @throws TypeErrorException
     */
    public function mergePaths(
        Paths $existingPaths,
        Paths $newPaths,
        OutputInterface $output,
        string $resolveConflicts = 'fail_on_conflict'
    ): Paths {
        $pathCopy = new Paths($existingPaths->getPaths());

        foreach ($newPaths->getPaths() as $pathName => $newPath) {
            $output->writeln(
                "Import path: " . $pathName,
                OutputInterface::VERBOSITY_VERBOSE
            );

            $existingPath = $pathCopy->getPath($pathName);
            if ($existingPath === null) {
                $output->writeln(
                    $pathName . " imported: it does not exists in base file.",
                    OutputInterface::VERBOSITY_VERBOSE
                );

                $pathCopy->addPath($pathName, $newPath);

                $output->writeln(
                    $pathName . "(all operations) added to output.",
                    OutputInterface::VERBOSITY_VERBOSE
                );
                continue;
            }

            foreach (self::PATH_OPERATIONS as $operation) {
                if (is_object($existingPath->{$operation})) {
                    $output->write(
                        "Warning: Path operation conflict : ",
                        false,
                        OutputInterface::VERBOSITY_VERBOSE
                    );
                    $output->writeln(
                        $pathName . "($operation) already exists in base file.",
                        OutputInterface::VERBOSITY_VERBOSE
                    );

                    switch ($resolveConflicts) {
                        case 'fail_on_conflict':
                            $output->writeln(
                                "Error : Resolve conflict to continue (fail_on_conflict)",
                                OutputInterface::VERBOSITY_NORMAL
                            );
                            exit();
                        case 'base_file':
                            $output->writeln(
                                "Keep the baseFile operation."
                                . " Additional file operation ignored (base_file)",
                                OutputInterface::VERBOSITY_VERBOSE
                            );
                            break;
                        case 'additional_files':
                            $output->writeln(
                                "BaseFile operation replaced by an additional file (additional_files)",
                                OutputInterface::VERBOSITY_VERBOSE
                            );
                            $existingPath->{$operation} = $newPath->{$operation};
                            $pathCopy->addPath($pathName, $existingPath);
                            break;
                        default:
                            $output->writeln(
                                "Error : cannot resolve conflict (" . $resolveConflicts . ")",
                                OutputInterface::VERBOSITY_NORMAL
                            );
                            exit();
                    }
                    continue;
                }

                if (!is_object($newPath->{$operation})) {
                    continue;
                }

                $existingPath->{$operation} = $newPath->{$operation};
                $pathCopy->addPath($pathName, $existingPath);

                $output->writeln(
                    $pathName . "($operation) added to output.",
                    OutputInterface::VERBOSITY_VERBOSE
                );
            }
        }

        return $pathCopy;
    }
}
