<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\spec\Components;
use cebe\openapi\spec\OpenApi;
use Ocus\OpenApiBuilder\FileHandling\Exception\IOException;
use Ocus\OpenApiBuilder\Merge\PathMergerInterface;
use Ocus\OpenApiBuilder\Merge\ReferenceNormalizer;
use Ocus\OpenApiBuilder\Reader\Exception\InvalidFileTypeException;
use Ocus\OpenApiBuilder\Reader\FileReader;
use Symfony\Component\Console\Output\OutputInterface;

use function array_merge;
use function array_push;
use function count;

class OpenApiImport implements OpenApiImportInterface
{
    public const MERGE_TYPES = [
        'fail_on_conflict',
        'base_file',
        'additional_files',
    ];

    private FileReader $openApiReader;

    private PathMergerInterface $pathMerger;
    private OpenApiFilterInterface $filterer;
    private ReferenceNormalizer $referenceNormalizer;

    public function __construct(
        FileReader $openApiReader,
        OpenApiFilterInterface $openApiFilter,
        PathMergerInterface $pathMerger,
        ReferenceNormalizer $referenceResolver
    ) {
        $this->openApiReader = $openApiReader;
        $this->filterer = $openApiFilter;
        $this->pathMerger = $pathMerger;
        $this->referenceNormalizer = $referenceResolver;
    }

    /**
     * @param OpenApi $openApi
     * @param array $additionalFiles
     * @param OutputInterface $output
     * @param array $excludeTags
     * @param array $includeTags
     * @param bool $resolveReferences
     * @param string $resolveConflicts
     * @return OpenApi
     * @throws InvalidFileTypeException
     * @throws IOException
     * @throws TypeErrorException
     */
    public function importFiles(
        OpenApi $openApi,
        array $additionalFiles,
        OutputInterface $output,
        array $excludeTags = [],
        array $includeTags = [],
        bool $resolveReferences = false,
        string $resolveConflicts = 'fail_on_conflict'
    ): OpenApi {
        $output->writeln(
            "Import files count : " . count($additionalFiles),
            OutputInterface::VERBOSITY_VERBOSE
        );

        $mergedOpenApiDefinition = $openApi;

        // use "for" instead of "foreach" to iterate over new added files
        for ($i = 0; $i < count($additionalFiles); $i++) {
            $additionalFile = $additionalFiles[$i];
            $additionalDefinition = $this->openApiReader->readFile($additionalFile, $resolveReferences)->getOpenApi();

            if (count($excludeTags) || count($includeTags)) {
                $additionalDefinition = $this->filterer->filterFile(
                    $additionalDefinition,
                    $output,
                    $excludeTags,
                    $includeTags
                );
            }

            if (!$resolveReferences) {
                $resolvedReferenceResult = $this->referenceNormalizer->normalizeInlineReferences(
                    $additionalFile,
                    $additionalDefinition
                );
                array_push($additionalFiles, ...$resolvedReferenceResult->getFoundReferenceFiles());
                $additionalDefinition = $resolvedReferenceResult->getNormalizedDefinition();
            }

            $mergedOpenApiDefinition->paths = $this->pathMerger->mergePaths(
                $mergedOpenApiDefinition->paths,
                $additionalDefinition->paths,
                $output,
                $resolveConflicts
            );

            if ($additionalDefinition->components === null) {
                $output->writeln(
                    "No components in additional file.",
                    OutputInterface::VERBOSITY_VERBOSE
                );
                continue;
            }

            if ($mergedOpenApiDefinition->components === null) {
                $mergedOpenApiDefinition->components = new Components([]);
                $output->writeln(
                    "No components in baseFile.",
                    OutputInterface::VERBOSITY_VERBOSE
                );
            }
            $conflictingSchemas = array_intersect_key(
                $mergedOpenApiDefinition->components->schemas,
                $additionalDefinition->components->schemas
            );

            if (count($conflictingSchemas)) {
                $output->write(
                    "Warning: Schema conflict : ",
                    false,
                    OutputInterface::VERBOSITY_VERBOSE
                );
                $output->writeln(
                    implode(" ", array_keys($conflictingSchemas))
                    . " already exists in base file.",
                    OutputInterface::VERBOSITY_VERBOSE
                );

                switch ($resolveConflicts) {
                    case 'fail_on_conflict':
                        $output->writeln(
                            "Error : Resolve conflict to continue (fail_on_conflict)",
                            OutputInterface::VERBOSITY_NORMAL
                        );
                        exit();
                    case 'base_file':
                        $output->writeln(
                            "Keep the baseFile schemas."
                            . " Additional file components ignored (base_file)",
                            OutputInterface::VERBOSITY_VERBOSE
                        );

                        $mergedOpenApiDefinition->components->schemas = array_merge(
                            $additionalDefinition->components->schemas ?? [],
                            $mergedOpenApiDefinition->components->schemas ?? []
                        );
                        break;
                    case 'additional_files':
                        $output->writeln(
                            "BaseFile schema replaced by an additional file (additional_files)",
                            OutputInterface::VERBOSITY_VERBOSE
                        );

                        $mergedOpenApiDefinition->components->schemas = array_merge(
                            $mergedOpenApiDefinition->components->schemas ?? [],
                            $additionalDefinition->components->schemas ?? []
                        );
                        break;
                    default:
                        $output->writeln(
                            "Error : cannot resolve conflict (" . $resolveConflicts . ")",
                            OutputInterface::VERBOSITY_NORMAL
                        );
                        exit();
                }
            } else {
                $mergedOpenApiDefinition->components->schemas = array_merge(
                    $mergedOpenApiDefinition->components->schemas ?? [],
                    $additionalDefinition->components->schemas ?? []
                );
            }
        }

        if ($resolveReferences && $mergedOpenApiDefinition->components !== null) {
            $mergedOpenApiDefinition->components->schemas = [];
        }

        return $mergedOpenApiDefinition;
    }
}
