<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder;

use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\FileHandling\SpecificationFile;
use Symfony\Component\Console\Output\OutputInterface;

interface OpenApiBuildInterface
{
    /**
     * @param File $baseFile
     * @param OutputInterface $output
     * @param array $additionalFiles
     * @param array $excludeTags
     * @param array $includeTags
     * @param bool $shouldRemoveUnusedTags
     * @param bool $shouldRemoveUnusedPolicies
     * @param bool $shouldRemoveUnusedSchemas
     * @param bool $shouldRemoveUnusedResponses
     * @param bool $resolveReferences
     * @param string $resolveConflicts
     * @return SpecificationFile
     */
    public function buildFiles(
        File $baseFile,
        OutputInterface $output,
        array $additionalFiles,
        array $excludeTags = [],
        array $includeTags = [],
        bool $shouldRemoveUnusedTags = false,
        bool $shouldRemoveUnusedPolicies = false,
        bool $shouldRemoveUnusedSchemas = false,
        bool $shouldRemoveUnusedResponses = false,
        bool $resolveReferences = false,
        string $resolveConflicts = 'fail_on_conflict'
    ): SpecificationFile;
}
