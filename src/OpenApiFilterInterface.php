<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder;

use cebe\openapi\spec\OpenApi;
use Symfony\Component\Console\Output\OutputInterface;

interface OpenApiFilterInterface
{
    /**
     * @param OpenApi $OpenApiDefinition
     * @param OutputInterface $output
     * @param array $excludeTags
     * @param array $includeTags
     * @return OpenApi
     */
    public function filterFile(
        OpenApi $OpenApiDefinition,
        OutputInterface $output,
        array $excludeTags,
        array $includeTags
    ): OpenApi;

    /**
     * @param OpenApi $OpenApiDefinition
     * @param OutputInterface $output
     * @param bool $shouldRemoveUnusedTags
     * @param bool $shouldRemoveUnusedPolicies
     * @param bool $shouldRemoveUnusedSchemas
     * @param bool $shouldRemoveUnusedResponses
     * @return OpenApi
     */
    public function cleanFile(
        OpenApi $OpenApiDefinition,
        OutputInterface $output,
        bool $shouldRemoveUnusedTags = false,
        bool $shouldRemoveUnusedPolicies = false,
        bool $shouldRemoveUnusedSchemas = false,
        bool $shouldRemoveUnusedResponses = false
    ): OpenApi;
}
