<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder;

use cebe\openapi\spec\OpenApi;
use Symfony\Component\Console\Output\OutputInterface;

class OpenApiCheck implements OpenApiCheckInterface
{
    public function checkFile(
        OpenApi $OpenApiDefinition,
        OutputInterface $output
    ): bool {
        if ($OpenApiDefinition->validate()) {
            $output->writeln(
                "File is an OpenApi valid definition",
                OutputInterface::VERBOSITY_VERBOSE
            );

            return true;
        }
        $output->writeln(
            "File is not is an OpenApi valid definition",
            OutputInterface::VERBOSITY_VERBOSE
        );

        $errors = $OpenApiDefinition->getErrors();
        $output->writeln(
            count($errors) . " errors found",
            OutputInterface::VERBOSITY_VERBOSE
        );

        foreach ($errors as $error) {
            $output->writeln(
                $error,
                OutputInterface::VERBOSITY_VERBOSE
            );
        }
        return false;
    }
}
