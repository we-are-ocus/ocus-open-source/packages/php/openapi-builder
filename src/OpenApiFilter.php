<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\spec\OpenApi;
use cebe\openapi\spec\Operation;
use cebe\openapi\spec\Paths;
use cebe\openapi\spec\Reference;
use Symfony\Component\Console\Output\OutputInterface;

class OpenApiFilter implements OpenApiFilterInterface
{
    /**
     * @throws TypeErrorException
     */
    public function filterFile(
        OpenApi $OpenApiDefinition,
        OutputInterface $output,
        array $excludeTags,
        array $includeTags
    ): OpenApi {
        $includedOperations = 0;
        $excludedOperations = 0;
        $pathsCount = count($OpenApiDefinition->paths);

        $output->writeln(
            "Exclude tags: " . count($excludeTags),
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "Include tags: " . count($includeTags),
            OutputInterface::VERBOSITY_VERBOSE
        );

        // include or exclude paths with specified tags
        $paths = new Paths([]);
        foreach ($OpenApiDefinition->paths->getPaths() as $pathName => $path) {
            $operations = [];
            foreach ($path->getOperations() as $j => $operation) {
                if (count($includeTags)) {
                    if (
                        count(array_intersect($operation->tags, $includeTags))
                        && !count(array_intersect($operation->tags, $excludeTags))
                    ) {
                        $operations[] = $operation;
                        $includedOperations++;
                    } else {
                        $excludedOperations++;
                    }
                } elseif (!count(array_intersect($operation->tags, $excludeTags))) {
                        $operations[] = $operation;
                        $includedOperations++;
                } else {
                    $excludedOperations++;
                }
            }
            if (count($operations)) {
                $path->operations = $operations;
                $paths->addPath($pathName, $path);
            }
        }
        $OpenApiDefinition->paths = $paths;

        // include or exclude tags
        $tags = [];
        foreach ($OpenApiDefinition->tags as $i => $tag) {
            if (count($includeTags)) {
                if (in_array($tag->name, $includeTags, true) && !in_array($tag->name, $excludeTags, true)) {
                    $tags[] = $tag ;
                }
            } elseif (! in_array($tag->name, $excludeTags, true)) {
                    $tags[] = $tag ;
            }
        }
        $OpenApiDefinition->tags = $tags;

        $output->writeln(
            "Paths in file : " . count($OpenApiDefinition->paths),
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "Paths excluded from file : " . ($pathsCount - count($OpenApiDefinition->paths)),
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "Operations in file : " . $includedOperations,
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "Operations excluded from file: " . $excludedOperations,
            OutputInterface::VERBOSITY_VERBOSE
        );

        return $OpenApiDefinition;
    }

    public function cleanFile(
        OpenApi $OpenApiDefinition,
        OutputInterface $output,
        bool $shouldRemoveUnusedTags = false,
        bool $shouldRemoveUnusedPolicies = false,
        bool $shouldRemoveUnusedSchemas = false,
        bool $shouldRemoveUnusedResponses = false
    ): OpenApi {
        $output->writeln(
            "Should remove :",
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "     Unused tags : " . ($shouldRemoveUnusedTags ? 'true' : 'false'),
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "     Unused policies : " . ($shouldRemoveUnusedPolicies ? 'true' : 'false'),
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "     Unused schemas : " . ($shouldRemoveUnusedSchemas ? 'true' : 'false'),
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "     Unused responses : " . ($shouldRemoveUnusedResponses ? 'true' : 'false'),
            OutputInterface::VERBOSITY_VERBOSE
        );

        $usedTags = [];
        $usedPolicies = [];
        $usedSchemas = [];
        $usedResponses = [];

        foreach ($OpenApiDefinition->paths as $i => $path) {
            foreach ($path->getOperations() as $j => $operation) {
                $usedTags = $this->getUsedTags(
                    $shouldRemoveUnusedTags,
                    $usedTags,
                    $operation
                );

                $usedResponses = $this->getUsedResponses(
                    $shouldRemoveUnusedResponses,
                    $usedResponses,
                    $operation
                );

                $usedPolicies = $this->getUsedPolicies(
                    $shouldRemoveUnusedPolicies,
                    $usedPolicies,
                    $operation
                );
            }

            $usedSchemas = $this->getUsedSchemas(
                $shouldRemoveUnusedSchemas,
                $usedSchemas,
                $path
            );
        }

        $this->removeUnusedTags(
            $shouldRemoveUnusedTags,
            $usedTags,
            $OpenApiDefinition,
            $output
        );

        $this->removeUnusedPolicies(
            $shouldRemoveUnusedPolicies,
            $usedPolicies,
            $OpenApiDefinition,
            $output
        );

        $this->removeUnusedResponses(
            $shouldRemoveUnusedResponses,
            $usedResponses,
            $OpenApiDefinition,
            $output
        );

        return $OpenApiDefinition;
    }

    /**
     * @param bool $shouldRemoveUnusedTags
     * @param $usedTags
     * @param Operation $operation
     * @return array|mixed|string[]
     */
    private function getUsedTags(bool $shouldRemoveUnusedTags, $usedTags, Operation $operation)
    {
        if ($shouldRemoveUnusedTags) {
            $usedTags = array_merge($usedTags, $operation->tags);
        }
        return $usedTags;
    }

    /**
     * @param bool $shouldRemoveUnusedResponses
     * @param array $usedResponses
     * @param Operation $operation
     * @return array
     */
    private function getUsedResponses(
        bool $shouldRemoveUnusedResponses,
        array $usedResponses,
        Operation $operation
    ): array {
        if (
            $shouldRemoveUnusedResponses
            && !empty($operation->responses)
            && is_array($operation->responses->getResponses())
        ) {
            foreach ($operation->responses->getResponses() as $response) {
                if ($response instanceof Reference) {
                    $usedResponses [] = substr(
                        $response->getReference(),
                        strlen('#/components/responses/')
                    );
                }
            }
        }
        return $usedResponses;
    }

    /**
     * @param bool $shouldRemoveUnusedPolicies
     * @param array $usedPolicies
     * @param Operation $operation
     * @return array
     */
    private function getUsedPolicies(bool $shouldRemoveUnusedPolicies, array $usedPolicies, Operation $operation): array
    {
        if ($shouldRemoveUnusedPolicies && is_array($operation->security)) {
            foreach ($operation->security as $security_requirement_index => $securityRequirement) {
                foreach ((array)$securityRequirement as $securityRequirementAttributeIndex => $security) {
                    if (is_array($security)) {
                        foreach ($security as $security_requirement => $securityRequirementData) {
                            $usedPolicies [] = $security_requirement;
                        }
                    }
                }
            }
        }
        return $usedPolicies;
    }

    /**
     * @param bool $shouldRemoveUnusedSchemas
     * @param array $usedSchemas
     * @param $path
     * @return array
     */
    public function getUsedSchemas(bool $shouldRemoveUnusedSchemas, array $usedSchemas, $path): array
    {
        if ($shouldRemoveUnusedSchemas) {
            print("Remove unused schemas not yet implemented\n");
            //foreach ($path->parameters as $parameter) {
                //$parameter->resolveReferences();
                // if(is_object($parameter->schema))print_r($parameter->schema);
                //if ($parameter instanceof Reference) {
                    //print(get_class_methods(Schema::class));
                   // $usedSchemas [] = $parameter;
                //}
            //}
        }
        return $usedSchemas;
    }

    /**
     * @param bool $shouldRemoveUnusedTags
     * @param array $usedTags
     * @param OpenApi $OpenApiDefinition
     * @param OutputInterface $output
     * @return void
     */
    public function removeUnusedTags(
        bool $shouldRemoveUnusedTags,
        array $usedTags,
        OpenApi $OpenApiDefinition,
        OutputInterface $output
    ): void {
        if ($shouldRemoveUnusedTags) {
            $tagsDefinition = [];
            $usedTags = array_unique($usedTags, SORT_REGULAR);

            foreach ($OpenApiDefinition->tags as $tag) {
                if (in_array($tag->name, $usedTags, true)) {
                    $tagsDefinition [] = $tag;
                    $output->writeln(
                        "Keep used tag : " . $tag->name,
                        OutputInterface::VERBOSITY_VERBOSE
                    );
                } else {
                    $output->writeln(
                        "Remove not used tag : " . $tag->name,
                        OutputInterface::VERBOSITY_VERBOSE
                    );
                }
            }

            $output->writeln(
                "Removed tag definitions: " . (count($OpenApiDefinition->tags) - count($tagsDefinition)),
                OutputInterface::VERBOSITY_VERBOSE
            );

            if (count($tagsDefinition)) {
                $OpenApiDefinition->tags = $tagsDefinition;
            } else {
                $OpenApiDefinition->tags = null;
            }

            $output->writeln(
                "Used tags: " . count($usedTags),
                OutputInterface::VERBOSITY_VERBOSE
            );

            $output->writeln(
                "Defined tags: " . count($OpenApiDefinition->tags),
                OutputInterface::VERBOSITY_VERBOSE
            );
        }
    }

    /**
     * @param bool $shouldRemoveUnusedPolicies
     * @param array $usedPolicies
     * @param OpenApi $OpenApiDefinition
     * @param OutputInterface $output
     * @return void
     */
    public function removeUnusedPolicies(
        bool $shouldRemoveUnusedPolicies,
        array $usedPolicies,
        OpenApi $OpenApiDefinition,
        OutputInterface $output
    ): void {
        if ($shouldRemoveUnusedPolicies) {
            $policiesDefinition = [];

            $uniqueUsedPolicies = array_unique($usedPolicies, SORT_REGULAR);

            $totalPolicies = count($OpenApiDefinition->components->securitySchemes);
            $output->writeln(
                "Total policies: " . $totalPolicies,
                OutputInterface::VERBOSITY_VERBOSE
            );

            foreach ($OpenApiDefinition->components->securitySchemes as $key => $securityScheme) {
                if (
                    in_array($key, $uniqueUsedPolicies, true)
                    || in_array($key, array('api', 'http', 'oAuth2'))
                ) {
                    $policiesDefinition[$key] = $securityScheme;
                }
            }
            $output->writeln(
                "Used policies: " . count($policiesDefinition),
                OutputInterface::VERBOSITY_VERBOSE
            );

            $OpenApiDefinition->components->securitySchemes = $policiesDefinition;
            $output->writeln(
                "Removed policies: "
                . ($totalPolicies - count($policiesDefinition)),
                OutputInterface::VERBOSITY_VERBOSE
            );
        }
    }

    /**
     * @param bool $shouldRemoveUnusedResponses
     * @param array $usedResponses
     * @param OpenApi $OpenApiDefinition
     * @param OutputInterface $output
     * @return void
     */
    public function removeUnusedResponses(
        bool $shouldRemoveUnusedResponses,
        array $usedResponses,
        OpenApi $OpenApiDefinition,
        OutputInterface $output
    ): void {
        if ($shouldRemoveUnusedResponses) {
            $responsesDefinition = [];

            $uniqueUsedResponses = array_unique($usedResponses, SORT_REGULAR);

            $totalResponses = count($OpenApiDefinition->components->responses);
            $output->writeln(
                "Total responses: " . $totalResponses,
                OutputInterface::VERBOSITY_VERBOSE
            );
            $output->writeln(
                "Used responses: " . count($uniqueUsedResponses),
                OutputInterface::VERBOSITY_VERBOSE
            );

            foreach ($OpenApiDefinition->components->responses as $key => $response) {
                if (in_array($key, $uniqueUsedResponses, true)) {
                    $responsesDefinition[$key] = $response;
                }
            }
            $OpenApiDefinition->components->responses = $responsesDefinition;

            $output->writeln(
                "Removed responses: "
                . ($totalResponses - count($uniqueUsedResponses)),
                OutputInterface::VERBOSITY_VERBOSE
            );
        }
    }
}
