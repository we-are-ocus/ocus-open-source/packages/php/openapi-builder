<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\FileHandling;

use Ocus\OpenApiBuilder\FileHandling\Exception\IOException;

use function dirname;
use function getcwd;
use function pathinfo;
use function realpath;

use const DIRECTORY_SEPARATOR;
use const PATHINFO_EXTENSION;

final class File
{
    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function getFileExtension(): string
    {
        return pathinfo($this->filename, PATHINFO_EXTENSION);
    }

    /**
     * @throws IOException
     */
    public function getAbsoluteFile(): string
    {
        $fullFilename = realpath($this->filename);
        if ($fullFilename === false) {
            throw IOException::createWithNonExistingFile(
                $this->createAbsoluteFilePath($this->filename)
            );
        }

        return $fullFilename;
    }

    /**
     * @throws IOException
     */
    public function getAbsolutePath(): string
    {
        return dirname($this->getAbsoluteFile());
    }

    private function createAbsoluteFilePath(string $filename): string
    {
        if (str_starts_with($filename, '/')) {
            return $filename;
        }

        return getcwd() . DIRECTORY_SEPARATOR . $filename;
    }
}
