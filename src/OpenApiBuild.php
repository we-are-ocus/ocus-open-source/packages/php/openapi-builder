<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder;

use Ocus\OpenApiBuilder\FileHandling\Exception\IOException;
use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\FileHandling\SpecificationFile;
use Ocus\OpenApiBuilder\Reader\Exception\InvalidFileTypeException;
use Ocus\OpenApiBuilder\Reader\FileReader;
use Symfony\Component\Console\Output\OutputInterface;

class OpenApiBuild implements OpenApiBuildInterface
{
    private FileReader $openApiReader;

    private OpenApiFilterInterface $filterer;

    private OpenApiImportInterface $importer;


    public function __construct(
        FileReader $openApiReader,
        OpenApiFilterInterface $openApiFilter,
        OpenApiImportInterface $openApiImporter
    ) {
        $this->openApiReader      = $openApiReader;
        $this->filterer           = $openApiFilter;
        $this->importer           = $openApiImporter;
    }

    /**
     * @throws InvalidFileTypeException
     * @throws IOException
     */
    public function buildFiles(
        File $baseFile,
        OutputInterface $output,
        array $additionalFiles,
        array $excludeTags = [],
        array $includeTags = [],
        bool $shouldRemoveUnusedTags = false,
        bool $shouldRemoveUnusedPolicies = false,
        bool $shouldRemoveUnusedSchemas = false,
        bool $shouldRemoveUnusedResponses = false,
        bool $resolveReferences = false,
        string $resolveConflicts = 'fail_on_conflict'
    ): SpecificationFile {
        $output->writeln(
            "Base file : " . $baseFile->getAbsoluteFile(),
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "Resolve reference : " . ((($resolveReferences) === true) ? 'true' : 'false'),
            OutputInterface::VERBOSITY_VERBOSE
        );
        $output->writeln(
            "Resolve conflicts : " . $resolveConflicts,
            OutputInterface::VERBOSITY_VERBOSE
        );


        $openApiDefinition = $this->openApiReader->readFile(
            $baseFile,
            $resolveReferences
        )->getOpenApi();

        if (count($excludeTags) || count($includeTags)) {
            $openApiDefinition = $this->filterer->filterFile(
                $openApiDefinition,
                $output,
                $excludeTags,
                $includeTags
            );
        }

        $openApiDefinition = $this->importer->importFiles(
            $openApiDefinition,
            $additionalFiles,
            $output,
            $excludeTags,
            $includeTags,
            $resolveReferences,
            $resolveConflicts
        );

        if (
            $shouldRemoveUnusedTags
            || $shouldRemoveUnusedPolicies
            || $shouldRemoveUnusedSchemas
            || $shouldRemoveUnusedResponses
        ) {
            $openApiDefinition = $this->filterer->cleanFile(
                $openApiDefinition,
                $output,
                $shouldRemoveUnusedTags,
                $shouldRemoveUnusedPolicies,
                $shouldRemoveUnusedSchemas,
                $shouldRemoveUnusedResponses
            );
        }

        return new SpecificationFile(
            $baseFile,
            $openApiDefinition
        );
    }
}
