<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Writer;

use Ocus\OpenApiBuilder\FileHandling\SpecificationFile;

interface DefinitionWriterInterface
{
    public function write(SpecificationFile $specFile): string;
}
