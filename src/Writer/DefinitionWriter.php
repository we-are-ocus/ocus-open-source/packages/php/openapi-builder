<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Writer;

use cebe\openapi\Writer;
use Ocus\OpenApiBuilder\FileHandling\SpecificationFile;
use Ocus\OpenApiBuilder\Writer\Exception\InvalidFileTypeException;

final class DefinitionWriter implements DefinitionWriterInterface
{
    /**
     * @throws InvalidFileTypeException
     */
    public function write(SpecificationFile $specFile): string
    {
        return match ($specFile->getFile()->getFileExtension()) {
            'json' => $this->writeToJson($specFile),
            'yml', 'yaml' => $this->writeToYaml($specFile),
            default => throw InvalidFileTypeException::createFromExtension($specFile->getFile()->getFileExtension()),
        };
    }

    public function writeToJson(SpecificationFile $specFile): string
    {
        return Writer::writeToJson($specFile->getOpenApi());
    }

    public function writeToYaml(SpecificationFile $specFile): string
    {
        return Writer::writeToYaml($specFile->getOpenApi());
    }
}
