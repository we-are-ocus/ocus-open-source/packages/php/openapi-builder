<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Console\Command;

use Ocus\OpenApiBuilder\FileHandling\Exception\IOException;
use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\FileHandling\Finder;
use Ocus\OpenApiBuilder\FileHandling\SpecificationFile;
use Ocus\OpenApiBuilder\OpenApiBuildInterface;
use Ocus\OpenApiBuilder\OpenApiCheckInterface;
use Ocus\OpenApiBuilder\OpenApiImport;
use Ocus\OpenApiBuilder\Writer\DefinitionWriterInterface;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use function array_map;
use function count;
use function file_put_contents;
use function is_string;
use function sprintf;
use function touch;

final class BuildCommand extends Command
{
    public const COMMAND_NAME = 'openapi-builder';

    private OpenApiBuildInterface $builder;
    private OpenApiCheckInterface $checker;
    private DefinitionWriterInterface $definitionWriter;
    private Finder $fileFinder;

    public function __construct(
        OpenApiBuildInterface $openApiBuild,
        OpenApiCheckInterface $openApiCheck,
        DefinitionWriterInterface $definitionWriter,
        Finder $fileFinder
    ) {
        parent::__construct(self::COMMAND_NAME);
        $this->builder = $openApiBuild;
        $this->checker = $openApiCheck;
        $this->definitionWriter = $definitionWriter;
        $this->fileFinder = $fileFinder;
    }

    protected function configure(): void
    {
        $this->setDescription('Build an OpenAPI definition file from one or more files')
            ->setHelp(
                <<<'HELP'
                Usage:
                    basefile.yml  > finalfile.yml

                Allowed extensions:
                    Only .yml, .yaml and .json files are supported

                Outputformat:
                    The output format is determined by the basefile extension.
                HELP
            )
            ->addArgument('basefile', InputArgument::REQUIRED)
            ->addArgument('additionalFiles', InputArgument::IS_ARRAY | InputArgument::OPTIONAL)
            ->addOption(
                'check',
                'c',
                InputOption::VALUE_NEGATABLE,
                'Check if file is valid.'
                . 'If additionalFiles are provided, the validation is done just before generate the output.'
            )
            ->addOption(
                'match',
                'm',
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'Use a RegEx pattern to determine the additionalFiles. '
                . 'If this option is set the additionalFiles will be omitted'
            )
            ->addOption(
                'exclude-tags',
                'x',
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                'Exclude path with one of its tags match one of the provided list.'
                . 'All path with one of this tags will be omitted'
            )
            ->addOption(
                'include-tags',
                'i',
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                'Filter tags on provided list.'
                . 'All path without one of this tags will be omitted'
            )
            ->addOption(
                'remove-unused-tags',
                null,
                InputOption::VALUE_NEGATABLE,
                'Removes all tags definition not used in path definitions. '
            )
            ->addOption(
                'remove-unused-policies',
                null,
                InputOption::VALUE_NEGATABLE,
                'Removes all policy definitions not used in path definitions. '
            )
            ->addOption(
                'remove-unused-schemas',
                null,
                InputOption::VALUE_NEGATABLE,
                'Removes all schema definitions not used in path definitions. '
            )
            ->addOption(
                'remove-unused-responses',
                null,
                InputOption::VALUE_NEGATABLE,
                'Removes all responses definitions not used in path definitions. '
            )
            ->addOption(
                'resolve-references',
                'r',
                InputOption::VALUE_NEGATABLE,
                'Resolve the "$refs" in the given files'
            )
            ->addOption(
                'resolve-conflicts',
                'f',
                InputOption::VALUE_REQUIRED,
                'Method used to resolve conflicts between files.
                    (fail_on_conflict, base_file or additional_files),
                    Default is fail_on_conflict.'
            )
            ->addOption(
                'config',
                null,
                InputOption::VALUE_REQUIRED,
                'Ini configuration file.'
            )
            ->addOption(
                'outputfile',
                'o',
                InputOption::VALUE_REQUIRED,
                'Defines the output file for the result. Defaults the result will printed to stdout'
            )
            ->addOption(
                'outputdir',
                'd',
                InputOption::VALUE_REQUIRED,
                'Defines the output directory for the result output files.'
            );
    }

    /**
     * @throws IOException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $baseFile = $input->getArgument('basefile');
        $additionalFiles = $input->getArgument('additionalFiles');

        if (count($additionalFiles) === 0) {
            foreach ($input->getOption('match') as $regex) {
                $additionalFiles = [...$additionalFiles, ...$this->fileFinder->find('.', $regex)];
            }
        }

        if (!is_string($baseFile)) {
            throw new RuntimeException('Invalid arguments given');
        }

        $initFile = $input->getOption('config');
        if ($initFile) {
            $ini = parse_ini_file($initFile);
            if (!$ini) {
                throw new RuntimeException('Not a valid ini file');
            }
        } else {
            $ini = [];
            $ini['check'] = null;
            $ini['exclude-tags'] = null;
            $ini['include-tags'] = null;
            $ini['remove-unused-tags'] = null;
            $ini['remove-unused-policies'] = null;
            $ini['remove-unused-schemas'] = null;
            $ini['remove-unused-responses'] = null;
            $ini['resolve-references'] = null;
            $ini['resolve-conflicts'] = null;
            $ini['outputdir'] = null;
            $ini['outputfile'] = null;
            $ini['verbose'] = null;
        }

        $withCheck = (bool) (
        ($input->getOption('check') !== null)
            ? $input->getOption('check') : $ini['check']
        );

        $excludeTags = $input->getParameterOption(['--exclude-tags', '-x']);
        if (!empty($excludeTags)) { //option is set
            $excludeTags = $input->getOption('exclude-tags');
            if (empty($excludeTags[0])) { // option set but no value
                $excludeTags = [];
            }
        } else { // option not set
            $excludeTags = (is_array($ini['exclude-tags']) ? $ini['exclude-tags'] : []);
        }

        $includeTags = $input->getParameterOption(['--include-tags', '-i']);
        if (!empty($includeTags)) { //option is set
            $includeTags = $input->getOption('include-tags');
            if (empty($includeTags[0])) { // option set but no value
                $includeTags = [];
            }
        } else { // option not set
            $includeTags = (is_array($ini['include-tags']) ? $ini['include-tags'] : []);
        }

        $shouldRemoveUnusedTags = (bool) (
        ($input->getOption('remove-unused-tags') !== null)
            ? $input->getOption('remove-unused-tags') : $ini['remove-unused-tags']
        );

        $shouldRemoveUnusedPolicies = (bool) (
        ($input->getOption('remove-unused-policies') !== null)
            ? $input->getOption('remove-unused-policies') : $ini['remove-unused-policies']
        );

        $shouldRemoveUnusedSchemas = (bool) (
        ($input->getOption('remove-unused-schemas') !== null)
            ? $input->getOption('remove-unused-schemas') : $ini['remove-unused-schemas']
        );

        $shouldRemoveUnusedResponses = (bool) (
        ($input->getOption('remove-unused-responses') !== null)
            ? $input->getOption('remove-unused-responses') : $ini['remove-unused-responses']
        );

        $shouldResolveReferences = (bool) (
            ($input->getOption('resolve-references') !== null)
            ? $input->getOption('resolve-references') : $ini['resolve-references']
        );

        $resolveConflicts =
            $input->getOption('resolve-conflicts')
                ?? $ini['resolve-conflicts'];
        $resolveConflicts =
            (!empty($resolveConflicts) && in_array($resolveConflicts, OpenApiImport::MERGE_TYPES, true))
                ? $resolveConflicts : 'fail_on_conflict';

        $outputFileDir = $input->getOption('outputdir') ?? $ini['outputdir'];
        $outputFileName = $input->getOption('outputfile') ?? $ini['outputfile'];
        if ($outputFileName) {
            $outputFileName = $outputFileDir . $outputFileName;
        }

        $verbose = $ini['verbose'];
        if ($verbose) {
            $output->setVerbosity(OutputInterface::VERBOSITY_VERBOSE);
        }

        $buildResult = $this->builder->buildFiles(
            new File($baseFile),
            $output,
            array_map(
                static fn(string $file): File => new File($file),
                $additionalFiles
            ),
            $excludeTags,
            $includeTags,
            $shouldRemoveUnusedTags,
            $shouldRemoveUnusedPolicies,
            $shouldRemoveUnusedSchemas,
            $shouldRemoveUnusedResponses,
            $shouldResolveReferences,
            $resolveConflicts
        );

        if ($withCheck) {
            $output->writeln(
                "Check output : true",
                OutputInterface::VERBOSITY_VERBOSE
            );
            $returnValue =
                $this->checker->checkFile($buildResult->getOpenApi(), $output) === true
                    ? Command::SUCCESS
                    : Command::INVALID;
        } else {
            $returnValue = Command::SUCCESS;
        }

        if ($outputFileName && is_string($outputFileName)) {
            if (str_starts_with($outputFileName, '/')) {
                $outputFileNameFull = $outputFileName;
            } else {
                $outputFileNameFull = dirname(__DIR__, 3) . '/' . $outputFileName;
            }
            if (!touch($outputFileNameFull)) {
                throw new RuntimeException('File cannot be created: ' . $outputFileNameFull);
            }
            $outputFile = new File($outputFileNameFull);
            $specificationFile = new SpecificationFile(
                $outputFile,
                $buildResult->getOpenApi()
            );
            file_put_contents(
                $outputFile->getAbsoluteFile(),
                $this->definitionWriter->write($specificationFile)
            );

            $output->writeln(
                sprintf('File successfully written to %s', $outputFile->getAbsoluteFile()),
                OutputInterface::VERBOSITY_VERBOSE
            );
        } else {
            $output->write($this->definitionWriter->write($buildResult), false, OutputInterface::VERBOSITY_NORMAL);
        }

        return $returnValue;
    }
}
