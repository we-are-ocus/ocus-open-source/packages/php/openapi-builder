<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Reader;

use cebe\openapi\spec\OpenApi;
use Exception;
use Ocus\OpenApiBuilder\FileHandling\Exception\IOException;
use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\FileHandling\SpecificationFile;
use Ocus\OpenApiBuilder\Reader\Exception\InvalidFileTypeException;

final class FileReader
{
    private OpenApiReaderWrapper $openApiReader;

    public function __construct(?OpenApiReaderWrapper $openApiReader = null)
    {
        $this->openApiReader = $openApiReader ?? new OpenApiReaderWrapper();
    }

    /**
     * @throws InvalidFileTypeException
     * @throws IOException
     * @throws Exception
     */
    public function readFile(File $inputFile, bool $resolveReferences = true): SpecificationFile
    {
        $spec = match ($inputFile->getFileExtension()) {
            'yml', 'yaml' => $this->openApiReader->readFromYamlFile(
                $inputFile->getAbsoluteFile(),
                OpenApi::class,
                $resolveReferences
            ),
            'json' => $this->openApiReader->readFromJsonFile(
                $inputFile->getAbsoluteFile(),
                OpenApi::class,
                $resolveReferences
            ),
            default => throw InvalidFileTypeException::createFromExtension($inputFile->getFileExtension()),
        };

        return new SpecificationFile(
            $inputFile,
            $spec
        );
    }
}
