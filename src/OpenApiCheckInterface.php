<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder;

use cebe\openapi\spec\OpenApi;
use Symfony\Component\Console\Output\OutputInterface;

interface OpenApiCheckInterface
{
    /**
     * @param OpenApi $OpenApiDefinition
     * @param OutputInterface $output
     * @return bool
     */
    public function checkFile(
        OpenApi $OpenApiDefinition,
        OutputInterface $output
    ): bool;
}
