<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder;

use cebe\openapi\spec\OpenApi;
use Symfony\Component\Console\Output\OutputInterface;

interface OpenApiImportInterface
{
    /**
     * @param OpenApi $openApi
     * @param array $additionalFiles
     * @param OutputInterface $output
     * @param array $excludeTags
     * @param array $includeTags
     * @param bool $resolveReferences
     * @param string $resolveConflicts
     * @return OpenApi
     */
    public function importFiles(
        OpenApi $openApi,
        array $additionalFiles,
        OutputInterface $output,
        array $excludeTags = [],
        array $includeTags = [],
        bool $resolveReferences = false,
        string $resolveConflicts = 'fail_on_conflict'
    ): OpenApi;
}
