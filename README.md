# OpenAPI Builder

Build OpenAPI 3.0.x YAML and JSON files.  

This application is a fork of [marcelthole/openapi-merge](https://github.com/marcelthole/openapi-merge)
([version 1.3](https://github.com/marcelthole/openapi-merge/releases/tag/1.3) )
and is based on [cebe/php-openapi](https://github.com/cebe/php-openapi) 

Because of its incompatibility with php 8, we have temporarily removed our dependency with the package cebe/php-openapi by including its files in an ext temp folder in our package and included its dependencies.
As soon as possible, we will remove this workaround and come back to the original package of cebe/php-openapi.
Actually, there is no visibility on the php 8 version of the cebe/php-openapi package.

The package is under development:
* Features are not all available yet
* Test coverage is not 100%

## Planned & available features :

### New features :

* Get stats on what is done during the openapi file build (Display params, results, processing details and error message)**- Available !**
* Remove unused tags, policies and unused responses **- Available !**
* Remove unused schemas **- Under development**
* Select or exclude specific tags **- Available !**
* Fail on multiple files conflicts and provide merging rules **- Available !**
* Full merge (not only paths but all components) **- Under development**
* Use a config file instead of command line arguments **- Available !**

### Original features :

(Marcelthole original package basic features, no change)
* Check if an openapi file is valid
* Give a list or a RegEx pattern to add ad additional files to merge with 
* Resolve the references by replacing references by the referenced values 

# Installation

```
composer require ocus/openapi-builder
```

# Usage
## CLI
```
$ vendor/bin/openapi-builder --help

Usage:
  openapi-builder [options] [--] <basefile> [<additionalFiles>...]

```

### Arguments
| Argument        | Meaning                         |
|-----------------|---------------------------------|
| basefile        | The yml or json file to build.  |
| additionalFiles | Additional files to merge with. |


### Options
| Option                             | Meaning                                                                                                                                   |
|------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| -c, --check                        | Check if file is valid.If additionalFiles are provided, the validation is done just before generate the output.                           |
| -m, --match [=MATCH]               | Use a RegEx pattern to determine the additionalFiles. If this option is set the additionalFiles will be omitted (multiple values allowed) |
| -x, --exclude-tags [=EXCLUDE-TAGS] | Exclude path with one of its tags match one of the provided list.All path with one of this tags will be omitted (multiple values allowed) |
| -i, --include-tags [=INCLUDE-TAGS] | Filter tags on provided list.All path without one of this tags will be omitted (multiple values allowed)                                  |
| --remove-unused-tags               | Removes all tags definition not used in path definitions.                                                                                 |
| --remove-unused-policies           | Removes all policy definitions not used in path definitions.                                                                              |
| --remove-unused-schemas            | Removes all schema definitions not used in path definitions.                                                                              |
| --remove-unused-responses          | Removes all responses definitions not used in path definitions.                                                                           |
| -r, --resolve-references           | Resolve the "$refs" in the given files                                                                                                    |
| -f, --resolve-conflicts [=METHOD]  | Resolve the conflicts with the given method (fail_on_conflict, base_file or additional_files). Default is fail_on_conflict.               |
| -o, --outputfile [=OUTPUTFILE]     | Defines the output file for the result. By default the result will printed to stdout                                                      |
| -d, --outputdir [=OUTPUTDIR]       | Defines the output dir for the output file.                                                                                               |
| --config [=INIFILE]                | Use a config file. Command lines parameters overrides the config file values.                                                             |
| -h, --help                         | Display help for the given command. When no command is given display help for the openapi-builder command                                 |
| -q, --quiet                        | Do not output any message                                                                                                                 |
| -V, --version                      | Display this application version                                                                                                          |
| --ansi, --no-ansi                  | Force (or disable --no-ansi) ANSI output                                                                                                  |
| -n, --no-interaction               | Do not ask any interactive question                                                                                                       |
| -v, --verbose                      | Increase the verbosity of messages: normal output displays errors only and verbose give details of treatments                             |


## Output-format

The output format is determined by the basefile extension.

## Config file

Here a sample of config file to run with --config. Command line overrides the config file if both are used.

```
check = false
match = false
exclude-tags[] = OneTag
exclude-tags[] = OneOtherTag
include-tags = false
remove-unused-tags = false
remove-unused-policies = false
remove-unused-schemas = false
remove-unused-responses = false
resolve-references = false
resolve-conflicts = base_file
outputfile = "openapi-build.yml"
outputdir=""
verbose = true
```

# Samples

Common uses cases.

#### Merge 2 files with a config file and write output in a specific directory
```
$ vendor/ocus/openapi-builder/bin/openapi-builder base.yml adds.yml -d ./results/
```

#### Merge 2 files in one file and keep the basefile parts in case of conflicts
```
$ vendor/ocus/openapi-builder/bin/openapi-builder base.yml adds.yml -f base_file -v -o result.yml
```

#### Merge 2 files in one file and keep the basefile parts in case of conflicts and keep only public tagged routes
```
$ vendor/ocus/openapi-builder/bin/openapi-builder base.yml adds.yml -f base_file -v -i public -o result.yml
```

#### Keep all except private tagged routes
```
$ vendor/ocus/openapi-builder/bin/openapi-builder base.yml -v -x private -o result.yml
```

#### Merge 3 files in one file, keep the add2.yml additional_file parts in case of conflicts
```
$ vendor/ocus/openapi-builder/bin/openapi-builder base.yml add1.yml add2.yml -f additional_files -v -o result.yml
```

#### Clean all unused components from a file 
```
$ vendor/ocus/openapi-builder/bin/openapi-builder openapi.yml --remove-unused-tags --remove-unused-policies --remove-unused-schemas --remove-unused-responses -v -o result.yml
```

#### Remove references
```
$ vendor/ocus/openapi-builder/bin/openapi-builder openapi.yml -v -r -o result.yml
```

#### Check if file is valid
```
$ vendor/ocus/openapi-builder/bin/openapi-builder openapi.yml -c -v
```

## Docker

Build the image locally from the sourcecode:
```
docker build --build-arg COMPOSER_REQUIRE_VERSION=<version> --no-cache -t ocus/openapi-builder:dev docker
docker run -v $PWD:/app --rm ocus/openapi-builder:dev [arguments]
```

## License

The Apache License (Apache 2.0). Please see [License File](LICENSE.md) for more information.
