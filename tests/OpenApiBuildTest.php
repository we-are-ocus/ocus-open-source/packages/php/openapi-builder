<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\spec\Components;
use cebe\openapi\spec\OpenApi;
use Ocus\OpenApiBuilder\FileHandling\Exception\IOException;
use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\Merge\PathMerger;
use Ocus\OpenApiBuilder\Merge\ReferenceNormalizer;
use Ocus\OpenApiBuilder\Merge\ReferenceResolverResult;
use Ocus\OpenApiBuilder\OpenApiBuild;
use Ocus\OpenApiBuilder\OpenApiFilter;
use Ocus\OpenApiBuilder\OpenApiImport;
use Ocus\OpenApiBuilder\Reader\Exception\InvalidFileTypeException;
use Ocus\OpenApiBuilder\Reader\FileReader;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Output\OutputInterface;

use function array_keys;

class OpenApiBuildTest extends TestCase
{
    /**
     * @throws InvalidFileTypeException
     * @throws IOException
     */
    public function testBuildPaths(): void
    {
        $sut = new OpenApiBuild(
            new FileReader(),
            new OpenApiFilter(),
            new OpenApiImport(new FileReader(), new OpenApiFilter(), new PathMerger(), new ReferenceNormalizer())
        );

        $result = $sut->buildFiles(
            new File(__DIR__ . '/Fixtures/base.yml'),
            $this->createMock(OutputInterface::class),
            [
                new File(__DIR__ . '/Fixtures/empty.yml'),
                new File(__DIR__ . '/Fixtures/routes.yml'),
                new File(__DIR__ . '/Fixtures/errors.yml'),
            ],
            [],
            [],
            false,
            false,
            false,
            false,
            false,
            'base_file'
        )->getOpenApi();

        self::assertCount(1, $result->paths->getPaths());
        self::assertNotNull($result->components);
        self::assertIsArray($result->components->schemas);
    }

    /**
     * @throws InvalidFileTypeException
     * @throws TypeErrorException
     * @throws IOException
     */
    public function testMergeFileWithoutComponents(): void
    {
        $openApiReader = new FileReader();
        $sut = new OpenApiImport(
            $openApiReader,
            new OpenApiFilter(),
            new PathMerger(),
            new ReferenceNormalizer()
        );

        $outputMock = $this->createMock(OutputInterface::class);

        $result = $sut->importFiles(
            $openApiReader->readFile(
                new File(__DIR__ . '/Fixtures/base-without-components.yml')
            )->getOpenApi(),
            [],
            $outputMock
        );

        self::assertNull($result->components);
    }

    /**
     * @throws TypeErrorException
     * @throws IOException
     * @throws InvalidFileTypeException
     */
    public function testReferenceNormalizer(): void
    {
        $referenceNormalizer = $this->createMock(ReferenceNormalizer::class);
        $referenceNormalizer->expects(
            self::exactly(2)
        )->method('normalizeInlineReferences')->willReturnCallback(static function (
            File $openApiFile,
            OpenApi $openApiDefinition
        ) {
            $foundReferences = [];
            if ($openApiFile->getAbsoluteFile() === __DIR__ . '/Fixtures/errors.yml') {
                $foundReferences[] = new File(__DIR__ . '/Fixtures/routes.yml');
            }

            return new ReferenceResolverResult(
                $openApiDefinition,
                $foundReferences
            );
        });

        $openApiReader = new FileReader();
        $sut = new OpenApiImport(
            $openApiReader,
            new OpenApiFilter(),
            new PathMerger(),
            $referenceNormalizer
        );

        $outputMock = $this->createMock(OutputInterface::class);

        $mergedResult = $sut->importFiles(
            $openApiReader->readFile(
                new File(__DIR__ . '/Fixtures/base.yml')
            )->getOpenApi(),
            [
                new File(__DIR__ . '/Fixtures/errors.yml'),
            ],
            $outputMock,
            [],
            [],
            false,
        );

        $mergedDefinition = $mergedResult;
        if ($mergedDefinition->components === null) {
            $mergedDefinition->components = new Components([]);
        }

        self::assertCount(1, $mergedDefinition->paths);
        self::assertSame(
            ['ProblemResponse', 'pingResponse'],
            array_keys($mergedDefinition->components->schemas)
        );
    }

    /**
     * @throws InvalidFileTypeException
     * @throws TypeErrorException
     * @throws IOException
     */
    public function testReferenceNormalizerWillNotBeExecuted(): void
    {
        $referenceNormalizer = $this->createMock(ReferenceNormalizer::class);
        $referenceNormalizer->expects(self::never())->method('normalizeInlineReferences');

        $openApiReader = new FileReader();
        $sut = new OpenApiImport(
            $openApiReader,
            new OpenApiFilter(),
            new PathMerger(),
            $referenceNormalizer
        );

        $outputMock = $this->createMock(OutputInterface::class);

        $sut->importFiles(
            $openApiReader->readFile(
                new File(__DIR__ . '/Fixtures/base.yml')
            )->getOpenApi(),
            [
                new File(__DIR__ . '/Fixtures/errors.yml'),
            ],
            $outputMock,
            [],
            [],
            true,
            'baseFile',
        );
    }
}
