<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\Merge;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\Writer;
use Ocus\OpenApiBuilder\FileHandling\Exception\IOException;
use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\Merge\ReferenceNormalizer;
use Ocus\OpenApiBuilder\Reader\Exception\InvalidFileTypeException;
use Ocus\OpenApiBuilder\Reader\FileReader;
use PHPUnit\Framework\TestCase;

class ReferenceNormalizerTest extends TestCase
{
    /**
     * @throws TypeErrorException
     * @throws IOException
     * @throws InvalidFileTypeException
     */
    public function testReadFileWithResolvedReference(): void
    {
        $file       = new File(__DIR__ . '/Fixtures/openapi-with-reference.json');
        $fileReader = new FileReader();
        $openApi    = $fileReader->readFile($file, false)->getOpenApi();

        $sut = new ReferenceNormalizer();

        $specificationResult = $sut->normalizeInlineReferences(
            $file,
            $openApi
        );

        self::assertStringEqualsFile(
            __DIR__ . '/Fixtures/expected/openapi-normalized.json',
            Writer::writeToJson($specificationResult->getNormalizedDefinition())
        );

        $foundRefFiles = $specificationResult->getFoundReferenceFiles();
        self::assertCount(3, $foundRefFiles);
        self::assertSame(
            __DIR__ . '/Fixtures/responseModel.json',
            $foundRefFiles[0]->getAbsoluteFile()
        );
        self::assertSame(
            __DIR__ . '/Fixtures/referenceModel.json',
            $foundRefFiles[1]->getAbsoluteFile()
        );
        self::assertSame(
            __DIR__ . '/Fixtures/sub/examples/referenceModel.json',
            $foundRefFiles[2]->getAbsoluteFile()
        );
    }
}
