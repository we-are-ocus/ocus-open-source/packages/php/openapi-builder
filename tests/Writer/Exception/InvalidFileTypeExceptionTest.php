<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\Writer\Exception;

use Ocus\OpenApiBuilder\Writer\Exception\InvalidFileTypeException;
use PHPUnit\Framework\TestCase;

class InvalidFileTypeExceptionTest extends TestCase
{
    public function testCreateException(): void
    {
        $exception = InvalidFileTypeException::createFromExtension('exe');
        self::assertSame('exe', $exception->getFileExtension());
        self::assertSame('The filetype "exe" is not supported for dumping', $exception->getMessage());
    }
}
