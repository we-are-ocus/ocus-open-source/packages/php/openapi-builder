<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\Writer;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\spec\OpenApi;
use Generator;
use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\FileHandling\SpecificationFile;
use Ocus\OpenApiBuilder\Writer\DefinitionWriter;
use Ocus\OpenApiBuilder\Writer\Exception\InvalidFileTypeException;
use PHPUnit\Framework\TestCase;

class DefinitionWriterTest extends TestCase
{
    /**
     * @dataProvider validSpecificationFiles
     * @throws InvalidFileTypeException
     */
    public function testWrite(SpecificationFile $specificationFile): void
    {
        $sut    = new DefinitionWriter();
        $result = $sut->write($specificationFile);
        self::assertNotEmpty($result);
    }

    /**
     * @return Generator
     * @throws TypeErrorException
     */
    public function validSpecificationFiles(): Generator
    {
        $specObject = new OpenApi([]);

        yield [
            new SpecificationFile(
                new File('dummy.yml'),
                $specObject
            ),
        ];

        yield [
            new SpecificationFile(
                new File('dummy.yaml'),
                $specObject
            ),
        ];

        yield [
            new SpecificationFile(
                new File('dummy.json'),
                $specObject
            ),
        ];
    }

    /**
     * @throws TypeErrorException
     */
    public function testWriteUnsupportedExtension(): void
    {
        $specificationFile = new SpecificationFile(
            new File('dummy.neon'),
            new OpenApi([])
        );

        $sut = new DefinitionWriter();

        $this->expectException(InvalidFileTypeException::class);
        $sut->write($specificationFile);
    }

    /**
     * @throws TypeErrorException
     */
    public function testWriteJson(): void
    {
        $specificationFile = new SpecificationFile(
            new File('dummy.json'),
            new OpenApi(['openapi' => '3.0.0'])
        );

        $sut = new DefinitionWriter();
        self::assertEquals(
            <<<'JSON'
            {
                "openapi": "3.0.0"
            }
            JSON,
            $sut->writeToJson($specificationFile)
        );
    }

    /**
     * @throws TypeErrorException
     */
    public function testWriteYaml(): void
    {
        $specificationFile = new SpecificationFile(
            new File('dummy.yml'),
            new OpenApi(['openapi' => '3.0.0'])
        );

        $sut = new DefinitionWriter();
        self::assertEquals(
            <<<'YML'
            openapi: 3.0.0

            YML,
            $sut->writeToYaml($specificationFile)
        );
    }
}
