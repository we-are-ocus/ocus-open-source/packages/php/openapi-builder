<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\Reader\Exception;

use Ocus\OpenApiBuilder\Reader\Exception\InvalidFileTypeException;
use PHPUnit\Framework\TestCase;

class InvalidFileTypeExceptionTest extends TestCase
{
    public function testCreateException(): void
    {
        $exception = InvalidFileTypeException::createFromExtension('exe');
        self::assertSame('exe', $exception->getFileExtension());
        self::assertSame('Given file has an unsupported file extension "exe"', $exception->getMessage());
    }
}
