<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\Reader;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\spec\OpenApi;
use Exception;
use Generator;
use Ocus\OpenApiBuilder\FileHandling\Exception\IOException;
use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\Reader\Exception\InvalidFileTypeException;
use Ocus\OpenApiBuilder\Reader\FileReader;
use Ocus\OpenApiBuilder\Reader\OpenApiReaderWrapper;
use PHPUnit\Framework\TestCase;

class FileReaderTest extends TestCase
{
    /**
     * @dataProvider validFilesDataProvider
     */
    public function testValidFiles(string $filename): void
    {
        $file          = new File($filename);
        $sut           = new FileReader();
        $specification = null;
        try {
            $specification = $sut->readFile($file);
        } catch (IOException | InvalidFileTypeException | Exception $e) {
            echo 'Exception : ',  $e->getMessage(), PHP_EOL;
        }
        self::assertSame($file, $specification->getFile());
    }

    /**
     * @return Generator
     */
    public function validFilesDataProvider(): Generator
    {
        yield [__DIR__ . '/Fixtures/valid-openapi.yml'];
        yield [__DIR__ . '/Fixtures/valid-openapi.yaml'];
        yield [__DIR__ . '/Fixtures/valid-openapi.json'];
    }

    /**
     * @throws IOException
     */
    public function testInvalidFile(): void
    {
        $sut  = new FileReader();
        $file = new File('openapi.neon');

        $this->expectException(InvalidFileTypeException::class);
        $sut->readFile($file);
    }

    /**
     * @throws IOException
     * @throws TypeErrorException
     * @throws InvalidFileTypeException
     */
    public function testPassResolveReference(): void
    {
        $dummyJsonFile = __DIR__ . '/Fixtures/valid-openapi.json';
        $dummyYamlFile = __DIR__ . '/Fixtures/valid-openapi.yml';

        $readerMock = $this->createMock(OpenApiReaderWrapper::class);
        $readerMock->expects(self::exactly(3))->method('readFromJsonFile')->withConsecutive(
            [$dummyJsonFile, OpenApi::class, true],
            [$dummyJsonFile, OpenApi::class, true],
            [$dummyJsonFile, OpenApi::class, false],
        )->willReturn(new OpenApi([]));

        $readerMock->expects(self::exactly(3))->method('readFromYamlFile')->withConsecutive(
            [$dummyYamlFile, OpenApi::class, true],
            [$dummyYamlFile, OpenApi::class, true],
            [$dummyYamlFile, OpenApi::class, false],
        )->willReturn(new OpenApi([]));

        $sut = new FileReader($readerMock);

        $sut->readFile(new File($dummyJsonFile));
        $sut->readFile(new File($dummyJsonFile), true);
        $sut->readFile(new File($dummyJsonFile), false);
        $sut->readFile(new File($dummyYamlFile));
        $sut->readFile(new File($dummyYamlFile), true);
        $sut->readFile(new File($dummyYamlFile), false);
    }
}
