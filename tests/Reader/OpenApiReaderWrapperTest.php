<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\Reader;

use cebe\openapi\spec\OpenApi;
use cebe\openapi\SpecObjectInterface;
use Exception;
use Ocus\OpenApiBuilder\Reader\OpenApiReaderWrapper;
use PHPUnit\Framework\TestCase;

class OpenApiReaderWrapperTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testCall(): void
    {
        $sut = new OpenApiReaderWrapper();
        self::assertInstanceOf(
            SpecObjectInterface::class,
            $sut->readFromJsonFile(
                __DIR__ . '/Fixtures/valid-openapi.json',
                OpenApi::class,
                true
            )
        );
        self::assertInstanceOf(
            SpecObjectInterface::class,
            $sut->readFromYamlFile(
                __DIR__ . '/Fixtures/valid-openapi.yml',
                OpenApi::class,
                true
            )
        );
    }
}
