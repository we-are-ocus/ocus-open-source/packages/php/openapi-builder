<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\FileHandling;

use cebe\openapi\spec\OpenApi;
use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\FileHandling\SpecificationFile;
use PHPUnit\Framework\TestCase;

class SpecificationFileTest extends TestCase
{
    public function testGetter(): void
    {
        $stubSpecObject = $this->createStub(OpenApi::class);
        $fileStub       = new File('example.file');
        $sut            = new SpecificationFile($fileStub, $stubSpecObject);

        self::assertSame($fileStub, $sut->getFile());
        self::assertSame($stubSpecObject, $sut->getOpenApi());
    }
}
