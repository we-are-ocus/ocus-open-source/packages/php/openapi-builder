<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\FileHandling\Exception;

use Ocus\OpenApiBuilder\FileHandling\Exception\IOException;
use PHPUnit\Framework\TestCase;

class IOExceptionTest extends TestCase
{
    public function testCreateException(): void
    {
        $exception = IOException::createWithNonExistingFile('dummyfile');
        self::assertSame('dummyfile', $exception->getFilename());
        self::assertSame('Given file "dummyfile" was not found', $exception->getMessage());
    }
}
