<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\Acceptance;

use PHPUnit\Framework\TestCase;

use function shell_exec;
use function sprintf;

class ConfigFileAcceptanceTest extends TestCase
{
    public function testConfigFile1(): void
    {
        $output = shell_exec(sprintf(
            'php %s --config %s ./tests/Acceptance/Fixtures/base.yml',
            __DIR__ . '/../../bin/openapi-builder',
            "./tests/Acceptance/Fixtures/openapi-config-1.ini"
        ));

        self::assertNotNull($output);

        self::assertSame(
            'Base file : ' . __DIR__ . '/Fixtures/base.yml
Resolve reference : false
Resolve conflicts : fail_on_conflict
Exclude tags: 2
Include tags: 0
Paths in file : 0
Paths excluded from file : 0
Operations in file : 0
Operations excluded from file: 0
Import files count : 0
File successfully written to ' . dirname(__DIR__, 2) . '/build/openapi-build.yml
',
            $output
        );
    }

    public function testConfigFile2(): void
    {
        $output = shell_exec(sprintf(
            'php %s --config %s ./tests/Acceptance/Fixtures/base.yml',
            __DIR__ . '/../../bin/openapi-builder',
            './tests/Acceptance/Fixtures/openapi-config-2.ini',
        ));

        self::assertNotNull($output);

        self::assertStringContainsString(
            'Base file : ' . __DIR__ . '/Fixtures/base.yml
Resolve reference : true
Resolve conflicts : base_file
Exclude tags: 0
Include tags: 1
Paths in file : 0
Paths excluded from file : 0
Operations in file : 0
Operations excluded from file: 0
Import files count : 0
Should remove :
     Unused tags : true
     Unused policies : true
     Unused schemas : true
     Unused responses : true
Removed tag definitions: 0
Used tags: 0
Defined tags: 0
Total policies: 0
Used policies: 0
Removed policies: 0
Total responses: 0
Used responses: 0
Removed responses: 0
Check output : true
',
            $output
        );
    }

    public function testConfigFileNoVerbose(): void
    {
        $output = shell_exec(sprintf(
            'php %s --config %s ./tests/Acceptance/Fixtures/base.yml',
            __DIR__ . '/../../bin/openapi-builder',
            './tests/Acceptance/Fixtures/openapi-config-2-no-v.ini',
        ));

        self::assertNull($output);
    }

    public function testConfigFileNoVerboseOverride(): void
    {
        $output = shell_exec(sprintf(
            'php %s -v --config %s ./tests/Acceptance/Fixtures/base.yml',
            __DIR__ . '/../../bin/openapi-builder',
            './tests/Acceptance/Fixtures/openapi-config-2-no-v.ini'
        ));

        self::assertNotNull($output);
    }

    public function testCommandArgumentsOverridesConfigFile2(): void
    {
        $output = shell_exec(sprintf(
            'php %s --config %s %s ./tests/Acceptance/Fixtures/base.yml',
            __DIR__ . '/../../bin/openapi-builder',
            './tests/Acceptance/Fixtures/openapi-config-2.ini',
            '--no-check'
            . ' --exclude-tags Private'
            . ' --exclude-tags Private2'
            . ' -i'
            . ' --no-remove-unused-tags'
            . ' --no-remove-unused-policies'
            . ' --no-remove-unused-schemas'
            . ' --no-remove-unused-responses'
            . ' --no-resolve-references'
            . ' --resolve-conflicts additional_files'
            . ' --outputfile openapi-build-new-name.yml'
            . ' --outputdir "build/cache/"'
            . ' -v '
        ));

        self::assertNotNull($output);

        self::assertSame(
            'Base file : ' . __DIR__ . '/Fixtures/base.yml
Resolve reference : false
Resolve conflicts : additional_files
Exclude tags: 2
Include tags: 0
Paths in file : 0
Paths excluded from file : 0
Operations in file : 0
Operations excluded from file: 0
Import files count : 0
File successfully written to ' . dirname(__DIR__, 2) . '/build/cache/openapi-build-new-name.yml
',
            $output
        );
    }


    public function testCommandArgumentsOverridesConfigFile1(): void
    {
        $output = shell_exec(sprintf(
            'php %s --config %s %s ./tests/Acceptance/Fixtures/base.yml',
            __DIR__ . '/../../bin/openapi-builder',
            './tests/Acceptance/Fixtures/openapi-config-2.ini',
            '-c'
            . ' --include-tags Special'
            . ' --include-tags Special2'
            . ' -x'
            . ' --remove-unused-tags'
            . ' --remove-unused-policies'
            . ' --remove-unused-schemas'
            . ' --remove-unused-responses'
            . ' --resolve-references'
            . ' --resolve-conflicts additional_files'
            . ' --outputfile openapi-build-new-name.yml'
            . ' --outputdir "build/cache/"'
            . ' -v '
        ));

        self::assertNotNull($output);

        self::assertStringContainsString(
            'Base file : ' . __DIR__ . '/Fixtures/base.yml
Resolve reference : true
Resolve conflicts : additional_files
Exclude tags: 0
Include tags: 2
Paths in file : 0
Paths excluded from file : 0
Operations in file : 0
Operations excluded from file: 0
Import files count : 0
Should remove :
     Unused tags : true
     Unused policies : true
     Unused schemas : true
     Unused responses : true
Removed tag definitions: 0
Used tags: 0
Defined tags: 0
Total policies: 0
Used policies: 0
Removed policies: 0
Total responses: 0
Used responses: 0
Removed responses: 0
Check output : true
',
            $output
        );
    }
}
