<?php

declare(strict_types=1);

namespace Ocus\OpenApiBuilder\Tests\Console\Command;

use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\spec\OpenApi;
use Exception;
use Generator;
use Ocus\OpenApiBuilder\Console\Command\BuildCommand;
use Ocus\OpenApiBuilder\FileHandling\File;
use Ocus\OpenApiBuilder\FileHandling\Finder;
use Ocus\OpenApiBuilder\FileHandling\SpecificationFile;
use Ocus\OpenApiBuilder\OpenApiBuildInterface;
use Ocus\OpenApiBuilder\OpenApiCheckInterface;
use Ocus\OpenApiBuilder\Writer\DefinitionWriterInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\TrimmedBufferOutput;

use function array_merge;
use function sprintf;
use function sys_get_temp_dir;
use function unlink;

use const PHP_EOL;

class ImportCommandTest extends TestCase
{
    /**
     * @dataProvider invalidArgumentsDataProvider
     * @throws Exception
     */
    public function testRunWithInvalidArguments(ArrayInput $input): void
    {
        $sut = new BuildCommand(
            $this->createStub(OpenApiBuildInterface::class),
            $this->createStub(OpenApiCheckInterface::class),
            $this->createStub(DefinitionWriterInterface::class),
            $this->createStub(Finder::class)
        );

        $output = new TrimmedBufferOutput(1024);

        $this->expectExceptionMessage('Invalid arguments given');
        $sut->run($input, $output);
    }

    /**
     * @return Generator<list<ArrayInput>>
     */
    public function invalidArgumentsDataProvider(): Generator
    {
        yield [
            new ArrayInput([
                'basefile' => null,
                'additionalFiles' => ['secondfile.yml'],
            ]),
        ];
    }

    /**
     * @throws Exception
     */
    public function testRun(): void
    {
        $baseFile   = new File('basefile.yml');
        $secondFile = new File('secondfile.yml');

        $buildResultStub = new SpecificationFile(
            new File('dummy'),
            $this->createStub(OpenApi::class)
        );

        $outputMock = new TrimmedBufferOutput(1024);

        $buildMock = $this->createMock(OpenApiBuildInterface::class);
        $buildMock->expects(self::once())
            ->method('buildFiles')
            ->with($baseFile, $outputMock, [$secondFile])
            ->willReturn($buildResultStub);

        $checkMock = $this->createMock(OpenApiCheckInterface::class);
        $checkMock->expects(self::never())
            ->method('checkFile')
            ->with($outputMock);

        $definitionWriterMock = $this->createMock(DefinitionWriterInterface::class);
        $definitionWriterMock->expects(self::once())
            ->method('write')
            ->with($buildResultStub)
            ->willReturn('dummy-write');

        $sut = new BuildCommand(
            $buildMock,
            $checkMock,
            $definitionWriterMock,
            $this->createStub(Finder::class)
        );

        $input  = new ArrayInput([
            'basefile' => 'basefile.yml',
            'additionalFiles' => ['secondfile.yml'],
        ]);
        $output = new TrimmedBufferOutput(1024);
        self::assertEquals(0, $sut->run($input, $output));

        self::assertSame('dummy-write', $output->fetch());
    }

    /**
     * @throws Exception
     */
    public function testRunWriteToFile(): void
    {
        $definitionWriterMock  = new class implements DefinitionWriterInterface {
            public function write(SpecificationFile $specFile): string
            {
                return 'dummy-data';
            }
        };

        $openApiCheckInterface = new class implements OpenApiCheckInterface {
            public function checkFile(
                OpenApi $OpenApiDefinition,
                OutputInterface $output
            ): bool {
                return true;
            }
        };

        $outputMock = new TrimmedBufferOutput(1024);
        $outputMock->setVerbosity(OutputInterface::VERBOSITY_VERBOSE);

        $openApiBuildInterface = new class implements OpenApiBuildInterface {
            public function buildFiles(
                File $baseFile,
                OutputInterface $output,
                array $additionalFiles,
                array $excludeTags = [],
                array $includeTags = [],
                bool $shouldRemoveUnusedTags = false,
                bool $shouldRemoveUnusedPolicies = false,
                bool $shouldRemoveUnusedSchemas = false,
                bool $shouldRemoveUnusedResponses = false,
                bool $resolveReferences = false,
                string $resolveConflicts = 'fail_on_conflict'
            ): SpecificationFile {
                return new SpecificationFile(
                    new File('dummy'),
                    new OpenApi([])
                );
            }
        };

        $sut = new BuildCommand(
            $openApiBuildInterface,
            $openApiCheckInterface,
            $definitionWriterMock,
            $this->createStub(Finder::class)
        );

        $tmpFile = sys_get_temp_dir() . '/merge-result.json';
        try {
            $input  = new ArrayInput([
                'basefile'        => 'basefile.yml',
                'additionalFiles' => ['secondfile.yml'],
                '-o'              => $tmpFile,
            ]);
            self::assertEquals(0, $sut->run($input, $outputMock));

            self::assertSame(
                sprintf('File successfully written to %s%s', $tmpFile, PHP_EOL),
                $outputMock->fetch()
            );
            self::assertStringEqualsFile($tmpFile, 'dummy-data');
        } finally {
            @unlink($tmpFile);
        }
    }

    /**
     * @return array<string, list<mixed>>
     */
    public function resolveReferenceArgumentDataProvider(): iterable
    {
        yield 'default-param' => [null, false];
        yield 'one as string' => ['1', true];
        yield 'zero as string' => ['0', false];
        yield 'true' => [true, true];
        yield 'false' => [false, false];
    }

    /**
     * @param string|bool|null $resolveReferenceValue
     *
     * @dataProvider resolveReferenceArgumentDataProvider
     * @throws TypeErrorException
     * @throws Exception
     */
    public function testResolveReferencesArgument(
        $resolveReferenceValue,
        bool $expectedResolveReferenceValue
    ): void {
        $basefile              = 'basefile.yml';
        $additionalFile        = 'secondfile.yml';
        $definitionWriterMock  = new class implements DefinitionWriterInterface {
            public function write(SpecificationFile $specFile): string
            {
                return 'dummy-data';
            }
        };

        $outputInterface = new TrimmedBufferOutput(1024);

        $openApiCheckInterface = $this->createMock(OpenApiCheckInterface::class);

        $openApiBuildInterface = $this->createMock(OpenApiBuildInterface::class);
        $openApiBuildInterface->method('buildFiles')->with(
            new File($basefile),
            $outputInterface,
            [new File($additionalFile)],
            [],
            [],
            false,
            false,
            false,
            false,
            $expectedResolveReferenceValue,
            'fail_on_conflict'
        )->willReturn(new SpecificationFile(
            new File($basefile),
            new OpenApi([])
        ));

        $sut = new BuildCommand(
            $openApiBuildInterface,
            $openApiCheckInterface,
            $definitionWriterMock,
            $this->createStub(Finder::class)
        );

        $arguments = [
            'basefile'             => $basefile,
            'additionalFiles'      => [$additionalFile],
        ];

        if ($resolveReferenceValue !== null) {
            $arguments['--resolve-references'] = $resolveReferenceValue;
        }

        $input  = new ArrayInput($arguments);
        $output = new TrimmedBufferOutput(1024);
        self::assertEquals(0, $sut->run($input, $output));
    }

    /**
     * @param array<string, list<string>> $arguments
     * @param list<File> $expectedFiles
     *
     * @dataProvider matchArgumentDataProvider
     * @throws TypeErrorException
     * @throws Exception
     */
    public function testMatchArgument(array $arguments, array $expectedFiles): void
    {
        $basefile = 'basefile.yml';

        $openApiBuildInterface = $this->createMock(OpenApiBuildInterface::class);
        $outputInterface = new TrimmedBufferOutput(1024);
        $openApiBuildInterface->method('buildFiles')->with(
            new File($basefile),
            $outputInterface,
            $expectedFiles
        )->willReturn(new SpecificationFile(
            new File($basefile),
            new OpenApi([])
        ));

        $sut    = new BuildCommand(
            $openApiBuildInterface,
            $this->createStub(OpenApiCheckInterface::class),
            $this->createStub(DefinitionWriterInterface::class),
            new class implements Finder {
                /** @return list<string> */
                public function find(string $baseDirectory, string $searchString): array
                {
                    return ['A.yml', 'B.yml'];
                }
            }
        );
        $input  = new ArrayInput(array_merge(['basefile' => $basefile], $arguments));
        $output = new TrimmedBufferOutput(1024);
        self::assertEquals(0, $sut->run($input, $output));
    }

    /**
     * @return iterable<string, array<string, mixed>>
     */
    public function matchArgumentDataProvider(): iterable
    {
        yield 'given additional files with match should ignore match' => [
            'arguments' => [
                'additionalFiles' => ['secondfile.yml'],
                '--match' => ['.*'],
            ],
            'expectedFiles' => [new File('secondfile.yml')],
        ];

        yield 'missing additionalFiles files with match should return match' => [
            'arguments' => [
                'additionalFiles' => [],
                '--match' => ['.*'],
            ],
            'expectedFiles' => [
                new File('A.yml'),
                new File('B.yml'),
            ],
        ];

        yield 'multiple matches return each match' => [
            'arguments' => [
                'additionalFiles' => [],
                '--match' => ['.*', '.*'],
            ],
            'expectedFiles' => [
                new File('A.yml'),
                new File('B.yml'),
                new File('A.yml'),
                new File('B.yml'),
            ],
        ];
    }
}
