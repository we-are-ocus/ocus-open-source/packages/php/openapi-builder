Because of its incompatibility with php 8, we have temporarily removed our dependency with the package cebe/php-openapi by including its files in an ext temp folder in our package and included its dependencies.
As soon as possible, we will remove this workaround and come back to the original package of cebe/php-openapi.
Actually, there is no visibility on the php 8 version of the cebe/php-openapi package.
